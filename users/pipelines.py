from models import Traveler
from django.contrib.auth.models import User, Permission, Group
from django.contrib.contenttypes.models import ContentType

GENDERS = {
  'male':'m',
  'female':'f',
}

def get_avatar(backend, strategy, details, response, user=None, *args, **kwargs):

  url = None
  if backend.name == 'facebook':
    url = "http://graph.facebook.com/%s/picture?type=large"%response['id']
  #if backend.name == 'twitter':
  #  url = response.get('profile_image_url', '').replace('_normal','')

  if url:
    traveler, traveler_created = Traveler.objects.get_or_create(user=user,gender=GENDERS[response.get('gender')])
    traveler.avatar = url
    traveler.save()

#set birthday and gender, and asociate user with Traveler model
def set_extra_info(backend, strategy, details, response, user, *args, **kwargs):
  #define content type for permissions, in this case is the Traveler model
  content_type = ContentType.objects.get_for_model(Traveler)

  #get or create group
  travelers, travelers_created = Group.objects.get_or_create(name='travelers')
  print 'travelers', travelers

  #get or create permissions
  can_see_gamification, can_see_gamification_created = Permission.objects.get_or_create(codename='can_see_gamification', name='user can see gamified profile', content_type=content_type)
  can_edit_gamification, can_edit_gamification_created = Permission.objects.get_or_create(codename='can_edit_gamification', name='user can edit gamified profile', content_type=content_type)

  #associate permissions to group
  travelers.permissions.add(can_see_gamification, can_edit_gamification)

  #add user to travelers groups
  user.groups.add(travelers)

  #get or create travler and asociate extra data with user 
  traveler, traveler_created  = Traveler.objects.get_or_create(user=user, gender=GENDERS[response.get('gender')])
  traveler.save()

  #the next lines are only for test

  # print 'user: ', user
  # print 'details: ', details
  # print 'anos: ', response
  # print 'genero: ', response.get('gender')
  # print 'timezone: ', response.get('timezone')

