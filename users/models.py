from django.db import models
from django.contrib.auth.models import User

class Traveler(models.Model):
  user = models.OneToOneField(User);
  GENDER_CHOICE = (
    ('M','Masculino'),
    ('F','Femenino'),
  )  
  birthdate   = models.DateTimeField(null = True)
  gender      = models.CharField(max_length=1, null=True, choices=GENDER_CHOICE)
  avatar      = models.CharField(max_length=200, null=True)
  