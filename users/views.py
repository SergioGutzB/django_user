from django.shortcuts import render_to_response, redirect, render
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required, permission_required
from django.views.generic import View
from .forms import ExtraDataForm 
# from django.template.context import RequestContext

class ExtraDataView(View):

  def get(self, request, *args, **kwargs):
    return redirect('/')
    #if request.user.status:
    #  return redirect('/')
    #else:
    #  return render(request, 'extra_data.html')

  def post(self, request, *args, **kwargs):
    form = ExtraDataForm(request.POST)
    if form.is_valid():
      request.user.username = request.POST['username']
      request.user.email = request.POST['email']      
      request.user.save()
      return render(request, 'home.html')
    else:
      error_username = form['username'].errors.as_text()
      error_email = form['email'].errors.as_text()
      ctx = {'error_username':error_username, 'error_email':error_email}
      return render(request, 'extra_data.html', ctx)


def login(request):  
    return render(request, 'login.html')

@permission_required('django_users.can_see_gamification', login_url='/')
def gamified_profile(request):
    return render(request, 'gamified.html');


def home(request):
    return render_to_response('home.html')


def logout(request):
    auth_logout(request)
    return redirect('/')

