from django.conf.urls import patterns, include, url
from django.contrib import admin
from users.views import ExtraDataView

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'user_register.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url('', include('social.apps.django_app.urls', namespace='social')),
    url(r'^$', 'users.views.login'),
    url(r'^home/$', 'users.views.home'),
    url(r'^logout/$', 'users.views.logout'),
    url(r'^gamified/$', 'users.views.gamified_profile'),
    url(r'^extra_data/$', ExtraDataView.as_view()),
)
